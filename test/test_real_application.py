import unittest
import subprocess
import requests
import os
import time


class TestRealApplication(unittest.TestCase):
    def setup_method(self, test_method):
        env = os.environ.copy()
        env["FLASK_APP"] = "./src/run.py"
        self.flask = subprocess.Popen([
            "flask",
            "run",
            "--host=0.0.0.0",
            ], env=env)
        time.sleep(1)

    def teardown_method(self, test_method):
        self.flask.kill()
        self.flask.wait()


    def test_returns_hello_world(self):
        request = requests.get('http://localhost:5000')
        assert request.status_code == requests.codes.ok
        assert request.text == "Hello World!"

if __name__ == '__main__':
    unittest.main()
