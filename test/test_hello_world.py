from src.project import app
import unittest

class TestHelloWorld(unittest.TestCase):
    def setup_method(self, test_method):
        self.app = app.test_client()

    def teardown_method(self, test_method):
        pass

    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
