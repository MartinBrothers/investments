""" This file displays a simple "Hello World" """

from flask import Flask, Blueprint
app = Flask(__name__)


blueprint = Blueprint('hello_world', __name__)

@blueprint.route('/')
def hello_world():
    return 'Hello World!'

if __name__ == "__main__":
    app.run()
