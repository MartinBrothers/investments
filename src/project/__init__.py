from flask import Flask
from src.hello_world import blueprint

app = Flask(__name__)
app.register_blueprint(blueprint)
