#!/bin/sh

set -eu

DEF_DOCKER_BUILD="yes"
DEF_FLASK_APP="src/run.py"
DEF_RUN_ARGS="flask run --host=0.0.0.0"
DEF_TAG_NAME="investmenttracker"
DEF_USE_CACHE=""
WORKDIR="/workdir"

e_err() {
    echo >&2 "ERROR: ${*}"
}

e_warn() {
    echo "WARN: ${*}"
}

usage() {
    echo "Usage: ${0} [OPTIONS] [COMMAND]"
    echo "This is a wrapper script around to run the python application in docker."
    echo "Its purpose is to have a simple access point to set environment variables."
    echo "  -a Set the entrypoint for the application (DEF_RUN_ARGS: '${DEF_RUN_ARGS}')"
    echo "  -b Build the dockerimge (DEF_DOCKER_BUILD: '${DEF_DOCKER_BUILD}')"
    echo "  -c Rebuild docker image without cache (DEF_USE_CACHE: '${DEF_USE_CACHE}')"
    echo "  -f Set the flask app (DEF_FLASK_APP: '${DEF_FLASK_APP}')"
    echo "  -t Set the tag of the docker (DEF_TAG_NAME: '${DEF_TAG_NAME}')"
    echo "All options can also be passed in environment variables (listed between [brackets])."
}

cleanup() {
	trap INT HUP
}

init() {
	trap cleanup INT HUP
}

build_image() {
    if [ ${build_docker:-${DEF_DOCKER_BUILD}} = "yes" ]; then
        docker build \
            ${use_cache} \
            --rm \
            --build-arg ROOTUID=$(id -u root) \
            --build-arg UID=$(id -u ${USER}) \
            --tag "${tag_name:-${DEF_TAG_NAME}}" \
            "./"
    fi
}

run_image() {
    docker run \
        --env FLASK_APP="${flask_app:-${DEF_FLASK_APP}}" \
        --interactive \
        --network=host \
        --rm \
        --tty \
        --volume "$(pwd)":${WORKDIR} \
        --workdir "${WORKDIR}" \
        "${tag_name:-${DEF_TAG_NAME}}" \
        ${run_args:-${DEF_RUN_ARGS}}
}

main() {
    use_cache=${DEF_USE_CACHE}
    while getopts ":a:b:cf:t:" _options; do
        case "${_options}" in
            a)
                run_args="${OPTARG}"
                ;;
            b)
                build_docker="${OPTARG}"
                ;;
            c)
                use_cache="--no-cache"
                ;;
            f)
                flask_app="${OPTARG}"
                ;;
            t)
                tag_name="${OPTARG}"
                ;;
            :)
                e_err "Option -${OPTARG} requires an argument."
                exit 1
                ;;
            ?)
                e_err "Invalid option: -${OPTARG}"
                exit 1
                ;;
        esac
    done
    shift "$((OPTIND - 1))"

    init
    build_image
    run_image
    cleanup
}

main "${@}"

exit 0
