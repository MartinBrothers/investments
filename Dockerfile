# Copyright (C) 2019 Martin Broers <broers.martin@gmail.com>

# For Alpine, latest is actually the latest stable
FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="Martin Broers <broers.martin@gmail.com>"

# We want the latest stable version from the repo
# hadolint ignore=DL3018
RUN \
    sed -i 's|\(^http.*\)community|&\n\1testing|' "/etc/apk/repositories" && \
    apk add --no-cache \
        dumb-init \
        py3-flask \
        py3-requests \
        pytest \
        pytest-cov \
        python3 \
        shadow \
        sudo \
    && \
    rm -rf "/var/cache/apk/"*

COPY "dockerfiles/docker-entrypoint.sh" "/init"

# User management
ARG ROOTUID=0
ARG UID=1000
RUN \
    echo '%wheel ALL=(ALL) ALL' > /etc/sudoers.d/wheel && \
    addgroup -g ${UID} build && \
    useradd --uid ${UID} \
      --gid ${UID} \
      --password "\$1\$bYE8dxaO\$ejiIY9TCz2n2I67.t2x6l0" \
      --create-home \
      --shell /bin/sh \
      build && \
    usermod -a -G wheel build && \
    usermod -a -G users build

USER build

ENTRYPOINT [ "/init" ]
